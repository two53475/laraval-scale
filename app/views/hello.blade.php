<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>LaraShare</title>
</head>
<body>

    @if(isset($url))
        <p>URL to image: <a href="{{ $url }}">{{ $url }}</a></p>
    @endif

    <br/>

	{{ Form::open(['files' => true]) }}
	    {{ Form::file('image') }}
	    {{ Form::submit('Upload!') }}
	{{ Form::close() }}
</body>
</html>