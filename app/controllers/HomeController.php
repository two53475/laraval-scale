<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function get()
	{
		return View::make('hello');
	}

	public function post()
	{
		// do all magic


		$sftp = new Net_SFTP(getenv('STORAGE_HOST'));

		if(!$sftp->login(getenv('STORAGE_USERNAME'), getenv('STORAGE_PASSWORD'))){
			// error
			dd('Error');
		}


		$file = Input::file('image');
		$name = $file->getClientOriginalName();

		$sftp->put("/home/forge/default/public/" .$name, $file);

		$url = "http://i.laravel-scale.tk/" . $name;
		return View::make('hello')->withURL($url);
	}


}
